import sys
import random

words = {}


def load_data():

    with open("datas.txt") as file:
        data = file.read().strip()

    split_data = data.split('\n')

    for entry in split_data:
        collate_the_list(entry)


def collate_the_list(line):
    split_line = line.split(' ')
    for i, word in enumerate(split_line):
        try:
            word_list = words[word]
        except KeyError:
            word_list = []
        try:
            word_list.append(split_line[i+1])
        except IndexError:
            pass
        words[word] = word_list


def generate():

    the_prompt = ''
    try:
        the_prompt = sys.argv[1]
    except IndexError:
        pass

    if the_prompt:
        print(f'your word: {the_prompt}')
        chain_it(words, the_prompt, the_prompt)


def chain_it(collection, the_word, result):
    # do the chain
    found = ''
    try:
        found_matches = collection[the_word]
        if found_matches:
            found = random.choice(found_matches)
            result = f'{result} {found}'
    except KeyError:
        print(f'no match for {the_word}')

    if found:
        chain_it(collection, found, result)
    else:
        print(result)


load_data()
generate()
